# kxid-me

kxid.me is a Keyoxide profile URL shortener

## Development

```bash
cargo run
# or
cargo watch -x 'run --bin kxid-me'
```