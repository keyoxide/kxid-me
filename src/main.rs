/*
Copyright 2022 Yarmo Mackenbach

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

use std::fs;
use serde::{Deserialize, Serialize};
use tera::Tera;
use actix_web::{
    body::BoxBody,
    dev::ServiceResponse,
    error,
    http::{header::ContentType, StatusCode},
    middleware::{self, ErrorHandlerResponse, ErrorHandlers},
    web, guard, App, Error, HttpRequest, HttpResponse, HttpServer, Result,
};
extern crate markdown;

#[derive(Debug, Deserialize, Serialize)]
struct Alias {
    alias: String,
    key_type: String,
    fingerprint: String,
    custom_query: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct Data {
    aliases: Vec<Alias>,
}

// Handle the index page
async fn index(
    tmpl: web::Data<tera::Tera>,
) -> Result<HttpResponse, Error> {
    let md_content = fs::read_to_string("./protocols/ariadne-alias-protocol-0.1.md").expect("Cannot read text file");
    let html : String = markdown::to_html(&md_content);

    let mut ctx = tera::Context::new();
    ctx.insert("ariadne_alias_protocol", &html);
    let body = tmpl.render("index.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("Template error"))?;
    Ok(HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body))
}

// Handle an alias request
async fn alias_handler(
    req: HttpRequest,
    tmpl: web::Data<tera::Tera>,
    path: web::Path<String>,
) -> Result<HttpResponse, Error> {
    if req.method() == "OPTIONS" {
        return Ok(HttpResponse::build(StatusCode::from_u16(204).unwrap())
            .append_header(("Allow", "GET, HEAD, OPTIONS"))
            .append_header(("Access-Control-Allow-Origin", "*"))
            .append_header(("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS"))
            .finish());
    }

    let data_content = fs::read_to_string("./data/data.json").expect("Cannot read JSON data file");
    let json_data: Data = serde_json::from_str(&data_content)?;

    let alias_to_find = path.into_inner();

    let index = json_data.aliases.iter().position(|r| &r.alias == &alias_to_find);

    match index {
        Some(i) => {
            let fingerprint = &json_data.aliases[i].fingerprint;
            Ok(HttpResponse::build(StatusCode::from_u16(301).unwrap())
                .append_header(("Location", format!("https://keyoxide.org/{fingerprint}")))
                .append_header(("Ariadne-Identity-Proof", format!("openpgp4fpr:{fingerprint}")))
                .append_header(("Access-Control-Allow-Origin", "*"))
                .append_header(("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS"))
                .finish())
        },
        None => {
            let mut ctx = tera::Context::new();
            ctx.insert("error", "Alias not found");
            ctx.insert("status_code", "404");
            let body = tmpl.render("error.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError("Template error"))?;
            Ok(HttpResponse::build(StatusCode::from_u16(404).unwrap())
                .content_type(ContentType::html())
                .body(body))
        }
    }
}

// Configure and run the web server
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    println!("Listening on 0.0.0.0:8080, starting server...");
    HttpServer::new(|| {
        let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();

        App::new()
            .app_data(web::Data::new(tera))
            .wrap(middleware::Logger::default()) // enable logger
            .wrap(middleware::DefaultHeaders::new().add(("Ariadne-Alias-Protocol", "0.1")))
            .service(web::resource("/{alias}")
                .route(web::route()
                    .guard(
                        guard::Any(guard::Get())
                            .or(guard::Head())
                            .or(guard::Options())
                    )
                    .to(alias_handler)))
            .service(web::resource("/")
                .route(web::get().to(index)))
            .service(web::scope("")
                .wrap(error_handlers()))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}

// Custom error handlers, to return HTML responses when an error occurs
fn error_handlers() -> ErrorHandlers<BoxBody> {
    ErrorHandlers::new().handler(StatusCode::NOT_FOUND, not_found)
}

// Error handler for a 404 Page not found error
fn not_found<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>> {
    let response = get_error_response(&res, "Page not found");
    Ok(ErrorHandlerResponse::Response(ServiceResponse::new(
        res.into_parts().0,
        response.map_into_left_body(),
    )))
}

// Generic error handler
fn get_error_response<B>(res: &ServiceResponse<B>, error: &str) -> HttpResponse {
    let request = res.request();

    // Provide a fallback to a simple plain text response in case an error occurs during the
    // rendering of the error page
    let fallback = |e: &str| {
        HttpResponse::build(res.status())
            .content_type(ContentType::plaintext())
            .body(e.to_string())
    };

    let tera = request.app_data::<web::Data<Tera>>().map(|t| t.get_ref());
    match tera {
        Some(tera) => {
            let mut context = tera::Context::new();
            context.insert("error", error);
            context.insert("status_code", res.status().as_str());
            let body = tera.render("error.html", &context);

            match body {
                Ok(body) => HttpResponse::build(res.status())
                    .content_type(ContentType::html())
                    .body(body),
                Err(_) => fallback(error),
            }
        }
        None => fallback(error),
    }
}
