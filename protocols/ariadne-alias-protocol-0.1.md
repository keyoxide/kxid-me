# Ariadne Alias Protocol

Author: Yarmo Mackenbach  
Version: 0.1  
Last updated: 2022-08-31

## Abstract

This specification describes a service to provide alternate or alias URLs for
Ariadne Identity profile pages and use those alias URLs as proof of identity to
verify identity claims.

## Status

This specification is a draft. Do not base implementations on this version of
the specification.

## Copyright notice

Copyright (c) 2022 Yarmo Mackenbach. All rights reserved.

## 1. Introduction

This specification describes an HTTP server that will forward chosen paths to
Ariadne Identity profile pages. It also describes the additional HTTP headers
and responses required to make these URLs work as proofs to verify identity
claims.

## 2. Justification

URLs for Ariadne Identity profile pages usually contain the fingerprint of the
cryptographic key that secures them. This can make them quite lengthy, hard to
remember and difficult to troubleshoot.

For example, here is the URL for the author's Keyoxide page:

https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d

An alias URL can be more meaningful and memorable. For example:

https://domain.tld/yarmo

Furthermore, identity proofs require the presence of the fingerprint of the
cryptographic key that secured the Ariadne Identity. Replacing the need for a
fingerprint with a short URL therefore also simplifies the process of making
identity proofs, and could prove useful when a service provider only provides
limited space for proofs.

## 3. Protocol requirements

To comply with the decentralized nature of the Ariadne Identity specification,
this protocol must avoid centralization and relying on known instances to
properly function. Any implementation supporting this protocol must be able to
reliably exchange aliases for fingerprints without having prior knowledge on the
domain that hosts the alias.

This protocol must also be simple enough that it can be implemented by just
configuring a reverse proxy. This allows both people with a domain and
alias-providing services to enjoy the protocol.

## 4. Ariadne Alias protocol for servers

### 4.1. Alias URL

An alias URL must follow [RFC2616](https://www.rfc-editor.org/rfc/rfc2616) and
be based on the HTTPS scheme. An alias URL may be the root of a domain or a path
on that domain. It may be a subdomain of the base domain.

The alias URL must accept the GET, HEAD and OPTIONS methods.

The following are valid alias URLs:

- https://domain.tld
- https://domain.tld/id
- https://domain.tld/name/id
- https://sub.domain.tld
- https://sub.domain.tld/id

The following are invalid alias URLs:

- http://domain.tld
- http://domain.tld/id

### 4.2. HTTP redirection

The alias URL must redirect directly to the location of an Ariadne Identity
profile page. The redirect must use the 301 HTTP status code.

### 4.3. HTTP headers

The alias URL must support cross-origin resource sharing (CORS) and therefore
return the following headers:

- Access-Control-Allow-Origin: *
- Access-Control-Allow-Methods: GET, HEAD, OPTIONS

The Access-Control-Allow-Methods header should at least contain the three
methods described above, more may be added.

## 5. Ariadne Alias protocol for clients

### 5.1. Identity claim verification process

To support aliases as proofs, the client should proceed as follows:

1. Request the proof data.
2. Find the location inside the data where the proof should be located.
3. Attempt to find the fingerprint.
4. If not found, scan the data for HTTPS URLs.
5. For each URL found:
    1. (optional) Perform an OPTIONS request, continue only if HEAD requests are
       allowed.
    2. Perform a HEAD request.
    3. If the response status is 301, follow the redirect and use that HTTP
       response in the next step.
    4. Scan the headers for the Ariadne-Identity-Proof header:
        1. If found, use that header's value as the proof.
        2. If not found, mark the claim as not verified.

This process only differs from the default process in step 5.3. where the URL
was an alias URL because it redirects to the actual profile page.

The client must not follow consecutive redirects, since the alias URL should
redirect directly to the profile page.