## Builder
FROM rust:latest AS builder

RUN rustup target add x86_64-unknown-linux-musl
RUN apt-get update && apt-get install -y musl-tools musl-dev
RUN update-ca-certificates

# Create appuser
ENV USER=kx
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /kxid-me

COPY ./ .

RUN cargo build --target x86_64-unknown-linux-musl --release

## Final image
FROM scratch

# Import from builder
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /kxid-me

# Copy our build and supporting files
COPY --from=builder /kxid-me/target/x86_64-unknown-linux-musl/release/kxid-me ./
COPY --from=builder /kxid-me/data ./data
COPY --from=builder /kxid-me/protocols ./protocols
COPY --from=builder /kxid-me/templates ./templates

# Use an unprivileged user
USER kx:kx

EXPOSE 8080

CMD ["/kxid-me/kxid-me"]